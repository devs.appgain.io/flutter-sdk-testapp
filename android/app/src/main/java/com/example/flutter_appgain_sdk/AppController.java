package com.appgain.flutter_sdk;

import android.content.Context;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import io.appgain.sdk.controller.Appgain;
import io.appgain.sdk.model.User;
import io.appgain.sdk.util.Config;


/**
 * Created by developers@appgain.io on 12/28/2021.
 */

public class AppController extends MultiDexApplication implements LifecycleObserver {

    public static final boolean DEBUG_MODE  =false;
    private static AppController mInstance;
    String TAG  = "AppController";
    public static boolean DIALOG_CANCELLABLE = true;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        Appgain.enableLog();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    void onAppBackgrounded() {
        Appgain.onAppBackgrounded();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    void onAppForegrounded() {
        Appgain.onAppForegrounded();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    public static synchronized AppController getInstance() {
        return mInstance;
    }

}
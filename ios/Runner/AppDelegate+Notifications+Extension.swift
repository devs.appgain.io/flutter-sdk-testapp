//
//  AppDelegate+Extension.swift
//  PragueNow
//
//  Created by Ragaie Alfy on 8/24/19.
//  Copyright © 2019 Ragaie Alfy. All rights reserved.
//

import Foundation
import UIKit

import UserNotificationsUI
import UserNotifications
import Appgain

extension AppDelegate {
    
    

    override func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
          
          print("DEVICE TOKEN = \(deviceToken)")
          print("Device token string ==> \(deviceToken.base64EncodedString())")
          Appgain.registerDevice(withToken: deviceToken)
          let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
          let token = tokenParts.joined()
          print("Device Token: \(token)")
        
      }
    
    
    func  registerForRemoteNotification(){
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            DispatchQueue.main.async {
                UIApplication.shared.registerUserNotificationSettings(settings)
            }
        }
        DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    
    
    override func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        if let url = response.notification.request.content.userInfo["url"] as? String{
            switch UIApplication.shared.applicationState {
            case .background, .inactive:
                    // background
                
                let deadlineTime = DispatchTime.now() + .seconds(8)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                    print("test")
                
                if  let urll = URL(string: url){
            
                    UIApplication.shared.open(urll) { (result) in
                        if result {
                           // The URL was delivered successfully!
                        }
                    }
                }
            }
                break
                case .active:
                    // foreground
                    if  let urll = URL(string: url){
                
                        UIApplication.shared.open(urll) { (result) in
                            if result {
                               // The URL was delivered successfully!
                            }
                        }
                    }

                    break
                default:
                    break
            }
            
        }

        
        Appgain.recordPushStatus(NotificationStatus.opened(), userInfo: response.notification.request.content.userInfo)
//        if Auth.auth().canHandleNotification(response.notification.request.content.userInfo) {
//            completionHandler()
//            return
//        }
    }
    
    override func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        completionHandler([.alert, .badge, .sound])
        Appgain.recordPushStatus(NotificationStatus.conversion(), userInfo: notification.request.content.userInfo)

    }
    
    override func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        if let url = userInfo["url"] as? String{
            switch UIApplication.shared.applicationState {
            case .background, .inactive:
                    // background
                
                let deadlineTime = DispatchTime.now() + .seconds(8)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                    print("test")
                
                if  let urll = URL(string: url){
            
                    UIApplication.shared.open(urll) { (result) in
                        if result {
                           // The URL was delivered successfully!
                        }
                    }
                }
            }
                break
                case .active:
                    // foreground
                    if  let urll = URL(string: url){
                
                        UIApplication.shared.open(urll) { (result) in
                            if result {
                               // The URL was delivered successfully!
                            }
                        }
                    }

                    break
                default:
                    break
            }
            
        }
        Appgain.handlePush(userInfo, for: application)
        
    }
   
    
    override func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        //silent notification for update content
      
        Appgain.handlePush(userInfo, for: application)
    }
    


}

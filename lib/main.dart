import 'dart:async';

import 'package:appgain_sdk/appgain_sdk.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: const MyHomePage(title: 'Flutter Appgain SDK'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    Timer.run(initUser);
  }

  initUser() {
    initSDKDialog();
  }

  initUserWithResultDialog() {
    Appgain()
        .initAppgainSDK(appId: appId, appKey: apiKey)
        .then((result) => showAlertDialog(result.toString()));
  }

  getUserId() {
    Appgain()
        .getUserId()
        .then((result) => showAlertDialog('UserId = ${result.toString()}'));
  }

  matchLink() {
    Appgain().matchLink().then((result) {
      showAlertDialog('MatchLink Response = ${result.toString()}');
      if (result != null) {
        (result['extra_data']);
        (result['extra_data']['params']);
        (result['extra_data']['userId']);
        (result['smart_link_primary']);
        (result['smart_link_url']);
        (result['smart_link_id']);
      }
    });
  }
  String? appId;
  String? apiKey;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
          child: Center(
            child: Column(children: [
              const SizedBox(height: 10),
              InvokeButton(
                  buttonName: 'INITIALIZE',
                  function: () => initUserWithResultDialog()),
              InvokeButton(buttonName: 'GET USER ID', function: () => getUserId()),
              InvokeButton(buttonName: 'MATCH LINK', function: () => matchLink()),
              InvokeButton(
                  buttonName: 'FIRE AUTOMATOR',
                  function: () => fireAutomatorDialog()),
              InvokeButton(
                  buttonName: 'FIRE AUTOMATOR WITH PERSONALIZATION',
                  function: () => fireAutomatorWithPersonalizationDialog()),
              InvokeButton(
                  buttonName: 'CANCEL AUTOMATOR',
                  function: () => cancelFireAutomatorDialog()),
              InvokeButton(
                  buttonName: 'ADD PURCHASE', function: () => logPurchaseDialog()),
              InvokeButton(
                  buttonName: "UPDATE USER ID",
                  function: () => updateUserIdDialog()),
              InvokeButton(
                  buttonName: 'UPDATE USER DATA',
                  function: () => updateUserDialog()),
              InvokeButton(
                  buttonName: 'ADD EMAIL CHANNEL',
                  function: () => emailNotificationChannelDialog()),
              InvokeButton(
                  buttonName: 'ADD SMS CHANNEL',
                  function: () => smsNotificationChannelDialog()),
              InvokeButton(
                  buttonName: 'LOG EVENT', function: () => logEventDialog()),
            ]),
          )), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  final _appIdController = TextEditingController();
  final _apiKeyController = TextEditingController();
  final _triggerPointController = TextEditingController();
  final _automatorKeyController = TextEditingController();
  final _automatorValueController = TextEditingController();
  final _newUserIdController = TextEditingController();
  final _emailNotificationChannelController = TextEditingController();
  final _smsNotificationChannelController = TextEditingController();
  final _productNameController = TextEditingController();
  final _amountController = TextEditingController();
  final _currencyController = TextEditingController();
  final _userEmailController = TextEditingController();
  final _userPhoneController = TextEditingController();
  final _enableNotificationChannelController = TextEditingController();
  final _textFieldController2 = TextEditingController();
  final _textFieldController1 = TextEditingController();
  final _typeController = TextEditingController();
  final _actionController = TextEditingController();
  final _logEventKeyController = TextEditingController();
  final _logEventValueController = TextEditingController();

  initSDKDialog() async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: const Text('Init Sdk'),
            content: //<Widget>[
            SizedBox(
              height: 100,
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: _appIdController,
                    textInputAction: TextInputAction.go,
                    decoration: inputDecoration("APP ID"),
                  ),
                  TextField(
                    controller: _apiKeyController,
                    textInputAction: TextInputAction.go,
                    decoration: inputDecoration("API KEY"),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () async {
                  appId = _appIdController.text;
                  apiKey = _apiKeyController.text;
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  fireAutomatorDialog() async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('FIRE AUTOMATOR FOR'),
            content: //<Widget>[
            SizedBox(
                height: 50,
                child: TextField(
                  controller: _triggerPointController,
                  textInputAction: TextInputAction.go,
                  //  keyboardType: TextInputType.text(),
                  decoration: inputDecoration("TRIGGER POINT NAME"),
                )),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () async {
                  Navigator.of(context).pop();
                  await Appgain()
                      .fireAutomator(triggerPoint: _triggerPointController.text)
                      .then((value) => showAlertDialog(value.toString()));
                },
              )
            ],
          );
        });
  }

  fireAutomatorWithPersonalizationDialog() async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('TRIGGER WITH PERSONALIZATION'),
            content: //<Widget>[
            SizedBox(
              height: 145,
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: _triggerPointController,
                    textInputAction: TextInputAction.go,
                    decoration: inputDecoration('TRIGGER POINT FOR'),
                  ),
                  TextField(
                    controller: _automatorKeyController,
                    textInputAction: TextInputAction.go,
                    //  keyboardType: TextInputType.text(),
                    decoration: inputDecoration('KEY'),
                  ),
                  TextField(
                    controller: _automatorValueController,
                    textInputAction: TextInputAction.done,
                    decoration: inputDecoration('VALUE'),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () async {
                  Navigator.of(context).pop();
                  var personalizationData = {
                    _automatorKeyController.text:
                    _automatorValueController.text,
                  };
                  await Appgain()
                      .fireAutomatorWithPersonalization(
                      triggerPoint: _triggerPointController.text,
                      personalizationMap: personalizationData)
                      .then((value) => showAlertDialog(value.toString()));
                },
              )
            ],
          );
        });
  }

  cancelFireAutomatorDialog() async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('CANCEL FIRE AUTOMATOR FOR'),
            content: //<Widget>[
            SizedBox(
                height: 50,
                child: TextField(
                  controller: _triggerPointController,
                  textInputAction: TextInputAction.done,
                  //  keyboardType: TextInputType.text(),
                  decoration: inputDecoration("TRIGGER POINT NAME"),
                )),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () async {
                  Navigator.of(context).pop();
                  await Appgain()
                      .cancelfireAutomator(
                      triggerPoint: _triggerPointController.text)
                      .then((value) => showAlertDialog(value.toString()));
                },
              )
            ],
          );
        });
  }

  updateUserIdDialog() async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('UPDATE USER ID'),
            content: //<Widget>[
            SizedBox(
                height: 50,
                child: TextField(
                  controller: _newUserIdController,
                  textInputAction: TextInputAction.done,
                  //  keyboardType: TextInputType.text(),
                  decoration: inputDecoration("NEW USER ID"),
                )),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () async {
                  Navigator.of(context).pop();
                  await Appgain()
                      .setuserId(userId: _newUserIdController.text)
                      .then((value) => showAlertDialog(value.toString()));
                },
              )
            ],
          );
        });
  }

  emailNotificationChannelDialog() async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('CREATE NOTIFICATION CHANNEL'),
            content: //<Widget>[
            SizedBox(
              height: 50,
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: _emailNotificationChannelController,
                    textInputAction: TextInputAction.go,
                    decoration: inputDecoration('ENTER EMAIL'),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () async {
                  Navigator.of(context).pop();
                  await Appgain()
                      .addNotificationChannel(
                    notificationType: 'email',
                    item: _emailNotificationChannelController.value.text,
                  )
                      .then((value) => showAlertDialog(value.toString()));
                },
              )
            ],
          );
        });
  }

  smsNotificationChannelDialog() async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('CREATE NOTIFICATION CHANNEL'),
            content: //<Widget>[
            SizedBox(
              height: 50,
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: _smsNotificationChannelController,
                    textInputAction: TextInputAction.done,
                    decoration: inputDecoration("ENTER MOBILE NUMBER"),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () async {
                  Navigator.of(context).pop();
                  await Appgain().addNotificationChannel(
                      notificationType: 'SMS',
                      item: _smsNotificationChannelController.value.text);
                },
              )
            ],
          );
        });
  }

  logPurchaseDialog() async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Add Purchase'),
            content: //<Widget>[
            SizedBox(
              height: 150,
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: _productNameController,
                    textInputAction: TextInputAction.go,
                    decoration: inputDecoration('product name'),
                  ),
                  TextField(
                    controller: _amountController,
                    textInputAction: TextInputAction.go,
                    keyboardType:
                    const TextInputType.numberWithOptions(decimal: true),
                    decoration: inputDecoration('amount'),
                  ),
                  TextField(
                    controller: _currencyController,
                    textInputAction: TextInputAction.done,
                    decoration: inputDecoration('currency'),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () async {
                  Navigator.of(context).pop();
                  await Appgain()
                      .addPurchase(
                      productName: _productNameController.text,
                      amount: _amountController.text == ''
                          ? 0
                          : double.parse(_amountController.text),
                      currency: _currencyController.text)
                      .then((value) => showAlertDialog(value.toString()));
                },
              )
            ],
          );
        });
  }

  updateUserDialog() async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('update user'),
            content: //<Widget>[
            SizedBox(
              height: 200,
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: _userEmailController,
                    textInputAction: TextInputAction.go,
                    //  keyboardType: TextInputType.text(),
                    decoration: inputDecoration('USER EMAIL'),
                  ),
                  TextField(
                    controller: _userPhoneController,
                    textInputAction: TextInputAction.next,
                    //  keyboardType: TextInputType.text(),
                    decoration: inputDecoration('phone'),
                  ),
                  TextField(
                    controller: _textFieldController1,
                    textInputAction: TextInputAction.next,
                    //  keyboardType: TextInputType.text(),
                    decoration: inputDecoration('field one'),
                  ),
                  TextField(
                    controller: _textFieldController2,
                    textInputAction: TextInputAction.done,
                    //  keyboardType: TextInputType.text(),
                    decoration: inputDecoration('field two'),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () async {
                  Navigator.of(context).pop();
                  var updatedData = {
                    'userEmail': _userEmailController.text,
                    'phone': _userPhoneController.text,
                    'updatedField1': _textFieldController1.text,
                    "updatedField2": _textFieldController2.text
                  };
                  await Appgain()
                      .updateUser(data: updatedData)
                      .then((value) => showAlertDialog(value.toString()));
                },
              )
            ],
          );
        });
  }

  enableNotificationChannelDialog() async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('enable notification channel'.toUpperCase()),
            content: //<Widget>[
            SizedBox(
              height: 50,
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: _enableNotificationChannelController,
                    textInputAction: TextInputAction.go,
                    decoration: inputDecoration("Notification Type"),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () async {
                  Navigator.of(context).pop();
                  await Appgain()
                      .enableReciveNotification(
                      type: _enableNotificationChannelController.text)
                      .then((value) => showAlertDialog(value.toString()));
                },
              )
            ],
          );
        });
  }

  logEventDialog() async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Log event'),
            content: //<Widget>[
            SizedBox(
                height: 200,
                child: Column(
                  children: <Widget>[
                    TextField(
                      controller: _typeController,
                      textInputAction: TextInputAction.go,
                      decoration: inputDecoration('Type'),
                    ),
                    TextField(
                      controller: _actionController,
                      textInputAction: TextInputAction.go,
                      decoration: inputDecoration('action'),
                    ),
                    TextField(
                      controller: _logEventKeyController,
                      textInputAction: TextInputAction.go,
                      decoration: inputDecoration('key'),
                    ),
                    TextField(
                      controller: _logEventValueController,
                      textInputAction: TextInputAction.done,
                      decoration: inputDecoration('value'),
                    ),
                  ],
                )),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () async {
                  Navigator.of(context).pop();
                  await Appgain().logEvent(
                    type: _typeController.value.text,
                    action: _actionController.value.text,
                    extras: {
                      _logEventKeyController.value.text:
                      _logEventValueController.value.text
                    },
                  ).then((value) => showAlertDialog(value.toString()));
                },
              )
            ],
          );
        });
  }

  showAlertDialog(String message) {
    return showDialog(
        context: context,
        useRootNavigator: false,
        builder: (_) => AlertDialog(
          title: const Text(''),
          content: Text(message),
          actions: [
            TextButton(
              child: const Text("OK"),
              onPressed: () {
                Navigator.pop(_);
              },
            )
          ],
        ));
  }

  InputDecoration inputDecoration(String? message) =>
      InputDecoration(hintText: message!);
}

class InvokeButton extends StatelessWidget {
  final String? buttonName;
  final Function function;

  const InvokeButton(
      {Key? key, required this.buttonName, required this.function})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      width: double.infinity - 20,
      child: OutlinedButton(
        child: Text(buttonName!.toUpperCase()),
        onPressed: () {
          function();
        },
        style: OutlinedButton.styleFrom(
          primary: Colors.white,
          backgroundColor: Colors.teal,
        ),
      ),
    );
  }
}
